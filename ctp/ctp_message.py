from datetime import datetime
from uuid import uuid4
import json


class Message:
    def __init__(self, attached_files, integration_id, request_id, ir):
        self.integration_id = integration_id
        self.attached_files = attached_files
        self.ir = ir
        self.request_id = request_id

        self.id = str(uuid4())
        self.txn_id = str(uuid4())

    @property
    def ir_item(self):
        return {
            "id": self.id,
            "txn_id": self.txn_id,
            "request_id": self.request_id,
            "integration_id": self.integration_id,
            "ts": datetime.utcnow().isoformat(),
            "message": json.dumps(self.ir),
        }

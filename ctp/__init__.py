import json
import os
from datetime import datetime, timedelta
from functools import reduce
from operator import getitem
from uuid import uuid4
from .ctp_message import Message

import boto3
from typing import List


class CTP:

    sfn_arn = "arn:aws:states:{}:{}:stateMachine:{}"
    _integration_config = None

    def __init__(self, integration_id, raw_request, messages: List[Message]):
        self.request = raw_request
        self.integration_id = integration_id
        self.request_id = str(uuid4())
        self.messages = messages

        self.ddb = boto3.resource("dynamodb")
        self.s3 = boto3.resource("s3")
        self.sfn = boto3.client("stepfunctions")

    @property
    def integration_config(self):
        if self._integration_config is None:
            self._integration_config = self.ddb.Table(
                "integration_config"
            ).get_item(Key={"integration_id": self.integration_id})
        return self._integration_config["Item"]["config"]

    def process(self):
        self.log_raw_http()
        self.save_attached_documents()

        self.process_messages()
        self.publish_event()

    def publish_event(self):
        for message in self.messages:
            boto3.client("lambda").invoke(
                FunctionName="meta_txn_updater",
                Payload=json.dumps(
                    {
                        "transaction_id": message.txn_id,
                        "integration_id": message.integration_id,
                        "type": "transaction_created",
                        "event_msg": "Transaction started",
                        "error": None,
                        "success": True,
                    }
                ),
            )

    def log_raw_http(self):
        expiration_date = datetime.utcnow() + timedelta(days=31)

        raw_request_bucket = f"{self.integration_id}-raw"

        self.s3.Object(raw_request_bucket, self.request_id).put(
            Body=self.request, Expires=expiration_date
        )

    def save_attached_documents(self):
        for m, message in enumerate(self.messages):
            for f, attached_file in enumerate(message.attached_files):
                s3_key = f"{self.request_id}_{attached_file['name']}"

                documents_bucket = f"{self.integration_id}-docs"

                self.s3.Object(documents_bucket, s3_key).put(
                    Body=attached_file["body"]
                )

                self.messages[m].attached_files[f]["s3_key"] = s3_key

    def process_messages(self):
        for message in self.messages:
            self.create_dynamodb_records(message.ir_item)

            self.sfn.start_execution(
                stateMachineArn=self.sfn_arn.format(
                    os.environ["AWS_DEFAULT_REGION"],
                    os.environ["AWS_ACCOUNT_ID"],
                    self.integration_id,
                ),
                name=f"{message.txn_id}",
                input=json.dumps({"id": message.id}),
            )

    def create_dynamodb_records(self, message):

        # saving transaction record
        self.ddb.Table("txns").put_item(
            Item={"txn_id": message["txn_id"], "txn_status": 0}
        )

        # saving IR as first record withing transaction
        self.ddb.Table("irs").put_item(Item=message)

        self.create_grouping_record(message)

    def create_grouping_record(self, IR_item):
        grouping_key = self.get_grouping_key(IR_item["message"])

        if grouping_key:
            self.ddb.Table("grouping").put_item(
                Item={
                    "id": str(uuid4()),
                    "integration_id": IR_item["integration_id"],
                    "group_id": grouping_key,
                    "item_id": IR_item["id"],
                }
            )

    def get_grouping_key(self, message):
        if "grouping" in self.integration_config:
            grouping = self.integration_config["grouping"]
            grouping_path = list(grouping.values())[0]["grouping_path"]

            grouping_key = reduce(getitem, grouping_path, json.loads(message))
            return grouping_key

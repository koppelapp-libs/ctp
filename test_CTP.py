from unittest import TestCase
from unittest import mock
from ctp import CTP
from ctp.ctp_message import Message
import json


class CTPTestCase(TestCase):
    def setUp(self):
        def get_config_item(**kwargs):
            return {"Item": {"config": self.config_item}}

        resources = dict(
            dynamodb=mock.Mock(
                Table=mock.Mock(
                    return_value=mock.Mock(get_item=get_config_item)
                )
            ),
            s3=mock.Mock(),
        )

        clients = dict(stepfunctions=mock.Mock())
        self.boto3_patcher = mock.patch(
            "ctp.boto3",
            mock.Mock(
                resource=lambda x: resources[x], client=lambda x: clients[x]
            ),
        )
        self.boto3_patcher.start()

        self.s3_put = (
            self.boto3_patcher.target.boto3.resource("s3").Object("").put
        )
        self.ddb_put = (
            self.boto3_patcher.target.boto3.resource("dynamodb")
            .Table("irs")
            .put_item
        )
        self.sfn_exec = self.boto3_patcher.target.boto3.client(
            "stepfunctions"
        ).start_execution

        self.messages = [
            Message(
                attached_files=[
                    {"name": "file1.txt", "body": b"content1"},
                    {"name": "file2.txt", "body": b"content2"},
                ],
                integration_id="i_id_1",
                request_id="r_id_1",
                ir={"x": 1},
            )
        ]

    def test_no_grouping(self):
        self.config_item = {}

        a = CTP(
            integration_id="i_id_1",
            raw_request=b"HTTP/1.1 ...",
            messages=self.messages,
        )
        a.process()

        assert self.s3_put.call_count == 3
        assert self.s3_put.call_args_list[0][1]["Body"] == b"HTTP/1.1 ..."
        assert self.s3_put.call_args_list[1][1]["Body"] == b"content1"
        assert self.s3_put.call_args_list[2][1]["Body"] == b"content2"

        assert self.ddb_put.call_count == 2
        ddb_args = self.ddb_put.call_args_list
        assert ddb_args[1][1]["Item"]["integration_id"] == "i_id_1"
        assert ddb_args[1][1]["Item"]["message"] == '{"x": 1}'

        assert self.sfn_exec.call_count == 1
        payload = json.loads(self.sfn_exec.call_args_list[0][1]["input"])
        assert payload["id"] == ddb_args[1][1]["Item"]["id"]

    def test_with_grouping(self):
        self.config_item = {"grouping": {"merge": {"grouping_path": "x"}}}
        a = CTP(
            integration_id="i_id_1",
            raw_request=b"HTTP/1.1 ...",
            messages=self.messages,
        )
        a.process()

        assert self.s3_put.call_count == 3
        assert self.s3_put.call_args_list[0][1]["Body"] == b"HTTP/1.1 ..."
        assert self.s3_put.call_args_list[1][1]["Body"] == b"content1"
        assert self.s3_put.call_args_list[2][1]["Body"] == b"content2"

        assert self.ddb_put.call_count == 3
        ddb_args = self.ddb_put.call_args_list
        assert ddb_args[1][1]["Item"]["integration_id"] == "i_id_1"
        assert ddb_args[1][1]["Item"]["message"] == '{"x": 1}'

        assert ddb_args[2][1]["Item"]["item_id"] == ddb_args[1][1]["Item"]["id"]
        assert ddb_args[2][1]["Item"]["group_id"] == 1

        assert self.sfn_exec.call_count == 1
        payload = json.loads(self.sfn_exec.call_args_list[0][1]["input"])
        assert payload["id"] == ddb_args[1][1]["Item"]["id"]

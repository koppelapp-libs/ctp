from distutils.core import setup

setup(
    name="ctp",
    version="1.0.1",
    description="KoppelApp Connector to Paltform module",
    author="Maksym Bendeberia",
    author_email="m.bendeberia@mintlab.nl",
    url="https://gitlab.com/koppelapp-libs/ctp.git",
    packages=["ctp"],
)
